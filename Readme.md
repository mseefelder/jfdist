# GPU Mesh Inflation with Jump Flooding

## A WebGL implementation

### [Try it!](http://www.lcg.ufrj.br/~mseefelder/jfdist/)

### Main references:

* [Fast Voronoi Diagrams and Distance Field Textures on the GPU With the Jump Flooding Algorithm](https://blog.demofox.org/2016/02/29/fast-voronoi-diagrams-and-distance-dield-textures-on-the-gpu-with-the-jump-flooding-algorithm/)
* [Jump Flooding in GPU with Applications to Voronoi Diagram and Distance Transform](http://www.comp.nus.edu.sg/~tants/jfa.html)
* [Teddy - Takeo Igarashi](http://www-ui.is.s.u-tokyo.ac.jp/~takeo/teddy/teddy.htm)