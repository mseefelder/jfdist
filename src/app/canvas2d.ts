import { Point } from './point3d';

export class Canvas2d {
  private ctx : CanvasRenderingContext2D;
  public canvas : HTMLCanvasElement;

  isDrawing: boolean = false;
  contours: Point[][] = [];

  constructor(c : HTMLCanvasElement) {
    this.canvas = c;
    this.ctx = this.canvas.getContext('2d');

    // draw white background
		this.ctx.fillStyle = "#FFFFFF";
    this.ctx.fillRect( 0, 0, this.canvas.width, this.canvas.height );
    
    // set stroke style
    this.ctx.strokeStyle = '#000000';
  }

  drawPoint(pt: Point, c?: string) {
    this.ctx.fillStyle = c ? c : "#000000";
    this.ctx.fillRect(pt.x,pt.y,1,1);
  }

  cleanContours() {
    this.contours = [];
  }

  drawElements() { 
    this.ctx.fillStyle = "#FFFFFF";
    this.ctx.fillRect( 0, 0, this.canvas.width, this.canvas.height );
    this.ctx.strokeStyle = '#000000';
    this.ctx.fillStyle = '#000000';

    this.ctx.beginPath();
    for(let contour of this.contours) {
      if (contour.length < 2)
        continue;
      this.ctx.moveTo(contour[0].x, contour[0].y);
      for(let i = 1; i < contour.length; i++) {
        this.ctx.lineTo(contour[i].x, contour[i].y);
      }
      this.ctx.stroke();
      this.ctx.fill();
    }
    this.ctx.closePath();
  }

  startDrawing() {
    this.drawElements();
    this.ctx.beginPath();
    let contour = this.contours[this.contours.length-1];
    this.ctx.moveTo(contour[0].x, contour[0].y);
  }

  updateDrawing() {
    this.ctx.strokeStyle = '#000000';
    let contour = this.contours[this.contours.length-1];
    if(contour.length > 1) {
      this.ctx.moveTo(contour[contour.length-2].x, contour[contour.length-2].y);
      this.ctx.lineTo(contour[contour.length-1].x, contour[contour.length-1].y);
      this.ctx.stroke();
    }
  }

  diffFromLast(pt : Point) : boolean {
    return (pt.x !== this.contours[this.contours.length-1][this.contours[this.contours.length-1].length-1].x) 
      && 
      (pt.y !== this.contours[this.contours.length-1][this.contours[this.contours.length-1].length-1].y);
  } 

  mouseDown(e: MouseEvent) {
    this.cleanContours();

    this.isDrawing = true;
    this.contours.push([]);

    let pt = new Point(e.offsetX+0.5, e.offsetY+0.5);
    this.contours[this.contours.length-1].push(pt);
    
    this.startDrawing();
  }

  mouseUp(e: MouseEvent) {
    this.isDrawing = false;
    
    let pt = new Point(e.offsetX+0.5, e.offsetY+0.5);
    if(this.diffFromLast(pt)) this.contours[this.contours.length-1].push(pt);
    
    this.updateDrawing();
    this.ctx.closePath();
    this.drawElements();
  }

  mouseMove(e: MouseEvent) {
    if(!this.isDrawing) {
      return;
    }

    let pt = new Point(e.offsetX+0.5, e.offsetY+0.5);
    if(this.diffFromLast(pt)) this.contours[this.contours.length-1].push(pt);

    this.updateDrawing();

  }

  resize(w: number, h: number) {
    this.canvas.width = w;
    this.canvas.height = h;
    this.ctx.fillStyle = "#FFFFFF";
    this.ctx.fillRect( 0, 0, this.canvas.width, this.canvas.height );
    this.drawElements();
  }

  getLastContour () : Array<Point> {
    return this.contours[0];
  }

  getMask () {
    let mask = this.ctx.getImageData(0, 0, this.canvas.width, this.canvas.height).data.filter((v, id)=>{return id%4==2;});
    // we need to mirror the mask in the y axis
    let inverted = new Uint8ClampedArray(mask.length); //allocate enough space
    let newY = 0;
    for(let y = this.canvas.height; y>=0; y--) {
      for(let x = 0; x<this.canvas.width; x++) {
        inverted[x+newY*this.canvas.height] = mask[x+y*this.canvas.height];
      }
      newY++;
    }
    return inverted;
  }

  drawVertices (contour: Array<Point>, steiner: Array<Point>) {
    this.ctx.fillStyle = "#FFFFFF";
    this.ctx.fillRect( 0, 0, this.canvas.width, this.canvas.height );
    for(let p of contour) {
      this.drawPoint(p);
    } 
    for(let p of steiner) {
      this.drawPoint(p, "green");
    }
  }

}
