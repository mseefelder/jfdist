import { tryCatch } from 'rxjs/util/tryCatch';
import * as poly2tri from 'poly2tri';
import { Point } from './point3d';

export class Inflate {
    private borderDist : Float32Array;
    private medialAxisDist : Float32Array;
    private mask : Uint8ClampedArray;
    private w : number;
    private h : number;
    private maxRadius : number;
    private minRadius : number;

    public contour : Array<Point>;
    public steiner : Array<Point>;
    public triangles : Array<poly2tri.Triangle>;

    constructor (w: number, h: number, borderPts: Array<Point>, borderDist: Float32Array, medialAxisDist: Float32Array, mask: Uint8ClampedArray) {
        this.w = w;
        this.h = h;
        this.borderDist = borderDist;
        this.medialAxisDist = medialAxisDist;
        this.mask = mask;
        this.contour = borderPts.map((pt,id)=>{return new Point(pt.x-0.5, this.w-pt.y-0.5, pt.z, id);});
        console.log("All data gathered for inflation!");
        this.inflate();
    }

    private index (x: number, y: number) : number {
        return x+y*this.h;
    }

    private computeInflation (distFromBorder: number, distFromMedialAxis: number) : number {
        let radius = distFromBorder+distFromMedialAxis;
        return Math.sqrt(radius*radius - distFromMedialAxis*distFromMedialAxis);
    }

    private computeSteiner () {

        this.steiner = [];
        let r = 4;
        let inflation = 0.0;
        let borderDist = 0.0;
        let medialAxisDist = 0.0;
        let id = 0;
        for (let i = 0; i<this.w; i+=r) {
            for (let j = 0; j<this.h; j+=r) {
                if(j === 0 && i%(r*2) === r) j+=r/2;
                id = this.index(i,j);
                borderDist = this.borderDist[id];
                if (borderDist >= 4.5 && this.mask[id]===0) {
                    medialAxisDist = this.medialAxisDist[id];
                    inflation = this.computeInflation(borderDist, medialAxisDist);
                    this.steiner.push(new Point(i, j, inflation, this.contour.length+this.steiner.length));
                }
            }
        }
    }
    
    inflate () : boolean {
        //console.log(this.contour);
        let context = new poly2tri.SweepContext(this.contour);
        //console.log("poly2tri context ready!");
        //console.log(this.borderDist);
        //this.maxRadius = this.borderDist.reduce((acc, curr, id) => {return (this.mask[id] < 255)?Math.max(acc, curr):acc;});
        //this.minRadius = this.borderDist.reduce((acc, curr, id) => {return (this.mask[id] < 255)?Math.min(acc, curr):acc;});
        //console.log(`minRadius: ${this.minRadius}, maxRadius: ${this.maxRadius}`);
        this.computeSteiner();
        //console.log(`Computed Steiner points!`);
        context.addPoints(this.steiner);
        //console.log(`Added steiner points to context. Triangulating...`);
        try {
            context.triangulate();
        } catch (error) {
            alert(`Try sketching again and avoid self intersections!`);
            console.warn("Error while triangulating: ", error.message);
            return false;
        }
        console.log(`Triangulation complete!`);
        this.triangles = context.getTriangles();
        return true;
    }
}
