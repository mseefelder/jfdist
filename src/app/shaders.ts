export class Shaders {
    public fragment : string;
    public vertex : string;

    constructor () {
        this.vertex = `
        void main() 
        {
            vec4 modelViewPosition = modelViewMatrix * vec4(position, 1.0);
            gl_Position = projectionMatrix * modelViewPosition;
        }
        `
        ;


        this.fragment = `
        uniform bool jumpFlood;
        uniform sampler2D sketch;
        uniform float imgWidth;
        uniform float imgHeight;
        uniform int step;

        void main() {            
            vec2 pos = gl_FragCoord.xy/vec2(imgWidth, imgHeight);
            vec2 offset = vec2(0, 0);
            vec4 sample = vec4(texture2D(sketch, (gl_FragCoord.xy+offset)/vec2(imgWidth, imgHeight)).rgb, 1.0);

            if(jumpFlood) {
                vec4 value = vec4(0.0,0.0,2.0,1.0);
                if(length(sample.rg) > 0.0)
                    value = vec4(sample.x, sample.y, length(sample.xy-pos), 1.0);

                offset = vec2(step,0);
                sample = vec4(texture2D(sketch, (gl_FragCoord.xy+offset)/vec2(imgWidth, imgHeight)).rgb, 1.0);
                if(length(sample.rg) > 0.0)
                    value = (length(sample.xy-pos) < value.z) ? vec4(sample.x, sample.y, length(sample.xy-pos), 1.0) : value;

                offset = vec2(-step,0);
                sample = vec4(texture2D(sketch, (gl_FragCoord.xy+offset)/vec2(imgWidth, imgHeight)).rgb, 1.0);
                if(length(sample.rg) > 0.0)
                    value = (length(sample.xy-pos) < value.z) ? vec4(sample.x, sample.y, length(sample.xy-pos), 1.0) : value;

                offset = vec2(0,step);
                sample = vec4(texture2D(sketch, (gl_FragCoord.xy+offset)/vec2(imgWidth, imgHeight)).rgb, 1.0);
                if(length(sample.rg) > 0.0)
                    value = (length(sample.xy-pos) < value.z) ? vec4(sample.x, sample.y, length(sample.xy-pos), 1.0) : value;

                offset = vec2(0,-step);
                sample = vec4(texture2D(sketch, (gl_FragCoord.xy+offset)/vec2(imgWidth, imgHeight)).rgb, 1.0);
                if(length(sample.rg) > 0.0)
                    value = (length(sample.xy-pos) < value.z) ? vec4(sample.x, sample.y, length(sample.xy-pos), 1.0) : value;

                offset = vec2(step,step);
                sample = vec4(texture2D(sketch, (gl_FragCoord.xy+offset)/vec2(imgWidth, imgHeight)).rgb, 1.0);
                if(length(sample.rg) > 0.0)
                    value = (length(sample.xy-pos) < value.z) ? vec4(sample.x, sample.y, length(sample.xy-pos), 1.0) : value;

                offset = vec2(step,-step);
                sample = vec4(texture2D(sketch, (gl_FragCoord.xy+offset)/vec2(imgWidth, imgHeight)).rgb, 1.0);
                if(length(sample.rg) > 0.0)
                    value = (length(sample.xy-pos) < value.z) ? vec4(sample.x, sample.y, length(sample.xy-pos), 1.0) : value;

                offset = vec2(-step,step);
                sample = vec4(texture2D(sketch, (gl_FragCoord.xy+offset)/vec2(imgWidth, imgHeight)).rgb, 1.0);
                if(length(sample.rg) > 0.0)
                    value = (length(sample.xy-pos) < value.z) ? vec4(sample.x, sample.y, length(sample.xy-pos), 1.0) : value;

                offset = vec2(-step,-step);
                sample = vec4(texture2D(sketch, (gl_FragCoord.xy+offset)/vec2(imgWidth, imgHeight)).rgb, 1.0);
                if(length(sample.rg) > 0.0)
                    value = (length(sample.xy-pos) < value.z) ? vec4(sample.x, sample.y, length(sample.xy-pos), 1.0) : value;
                
                gl_FragColor = value;
            }
            else {
                gl_FragColor = (length(sample.rgb) > 1e-7) ? vec4(pos.x, pos.y, 0.0, 1.0) : vec4(0.0,0.0,0.0,0.0);
            }
        }
        `
        ;
    }
}
