uniform int pass;

uniform bool jumpFlood;
uniform bool medialAxis;

uniform sampler2D sketch;
uniform float imgWidth;
uniform float imgHeight;
uniform int step;

#include ./chunks/jumpflood.glsl;
#include ./chunks/medialaxis.glsl;

float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main() {
    vec2 coord = gl_FragCoord.xy;
    vec2 pos = coord/vec2(imgWidth, imgHeight);
    vec4 sample = vec4(texture2D(sketch, pos).rgb, 1.0);

    if (pass == 0) { // Cleanup
        int count = 0;
        vec4 neig = vec4(0.0);
        for (int x = -1; x<2; x++) {
            for (int y = -1; y<2; y++) {
                if(x==0 && y==0) continue;
                neig = texture2D( sketch, (coord+vec2(x,y))/vec2(imgWidth, imgHeight) );
                count += int(neig.r> 0.0);
            }
        }
        gl_FragColor = (count > 2) ? vec4(1.0) : vec4(0.0);
    }
    else if(pass == 1){ // Labeling
        gl_FragColor = (length(sample.rgb) > 0.0) ? vec4(gl_FragCoord.x, gl_FragCoord.y, 0.0, 1.0) : vec4(0.0);
    }
    else if(pass == 2) { // Jump flood
        gl_FragColor = jF(sketch, imgWidth, imgHeight);
    }
    else if(pass == 3) { // Medial Axis
        gl_FragColor = mA(sketch, imgWidth, imgHeight);
    }
    else if (pass == 4) { // Distance Field
        float d = sample.z/dist(vec2(imgWidth, imgHeight), vec2(0.0));
        gl_FragColor = vec4(d,d,d,1.0);
    }
    else if (pass == 5) { // Direct render
        gl_FragColor = sample;
    }
    else if (pass == 6) { // Smooth border
        if(sample.z == 0.0) {
            gl_FragColor = sample;
        }
        else{
            float count = 0.0;
            float accum = 0.0;
            vec4 neig = vec4(0.0);
            for (int x = -16; x<17; x++) {
                for (int y = -16; y<17; y++) {
                    neig = texture2D( sketch, (coord+vec2(x,y))/vec2(imgWidth, imgHeight) );
                    count += 1.0;//float(neig.z > 0.0);
                    accum += neig.z;
                }
            }
            gl_FragColor = vec4(sample.xy, accum/count, 1.0);
        }
        
    }
    else if (pass == 7) { // Smooth axis
        float count = 0.0;
        float accum = 0.0;
        vec4 neig = vec4(0.0);
        for (int x = -16; x<17; x++) {
            for (int y = -16; y<17; y++) {
                neig = texture2D( sketch, (coord+vec2(x,y))/vec2(imgWidth, imgHeight) );
                count += 1.0;//float(neig.z > 0.0);
                accum += neig.z;
            }
        }
        gl_FragColor = vec4(sample.xy, accum/count, 1.0);
        
    }
}