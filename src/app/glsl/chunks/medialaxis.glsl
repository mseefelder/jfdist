#define M_PI 3.1415926535897932384626433832795

float angleBetween(vec2 vectorA, vec2 vectorB) {
    float angle = atan(vectorA.y, vectorA.x) - atan(vectorB.y,  vectorB.x);
    angle = angle + float(angle < 0.0)*2.0*M_PI;
    return angle;
    //return acos(dot(vectorA, vectorB));
}

vec4 mA(sampler2D s, float iwidth, float iheight) {
    vec2 coord = gl_FragCoord.xy;
    vec2 pos = coord/vec2(iwidth, iheight);
    vec4 read = texture2D(s, pos);
    vec2 sample = read.rg;

    vec2 sampleDir = normalize(sample-coord);
    
    vec2 offset = vec2(0, 0);
    float isAxisA = 0.0;
    float isAxisB = 0.0;
    float isAxisC = 0.0;
    vec3 neig = vec3(0.0);
    vec2 neigDir = vec2(0.0);
    vec2 diff = vec2(0.0);
    float angle = 0.0;
    
    for (int x = -1; x<2; x++){
        for (int y = -1; y<2; y++) {
            if ((x==0 && y==0)) continue;
            offset = vec2( float(x),  float(y));
            neig = texture2D(s, (coord+offset)/vec2(iwidth, iheight)).rgb;
            neigDir = normalize(neig.xy-coord);

            angle = angleBetween(sampleDir, neigDir);
            diff = abs(neig.rg-sample);

            if(diff.x > 1. || diff.y > 1.) {isAxisA += 1.0;}
            if(angle > M_PI*0.5 && angle < M_PI*1.5) {isAxisB = 1.0;}
            if(read.b > neig.b) {isAxisC += 1.0;}
        }
    }

    isAxisA = clamp((isAxisA-1.0),0.0,1.0);
    
    if(read.b <= 2.5) isAxisC = 0.0;
    isAxisC = clamp((isAxisC-3.0),0.0,1.0);

    isAxisA = (isAxisA+isAxisB+isAxisC > 2.9) ? 1.0 : 0.0;
    return vec4(isAxisA, isAxisA, isAxisA, 1.0);
}