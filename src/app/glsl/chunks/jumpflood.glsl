// TODO: Investigate the use of Manhattan distance instead of euclidean in
// fragment passes.
float dist(vec2 a, vec2 b) {
    return length(b-a);//(b.x-a.x)*(b.x-a.x)+(b.y-a.y)*(b.y-a.y);//
}

vec4 jF (sampler2D s, float iwidth, float iheight) {
    vec2 coord = gl_FragCoord.xy;
    vec2 pos = coord/vec2(iwidth, iheight);
    vec3 sample = texture2D(s, pos).rgb;
    // TODO: Replace dist(...) by length(vec2(iwidth, iheight)) here
    vec4 value = vec4(0.0,0.0,dist(vec2(iwidth, iheight), vec2(0.0)),1.0);
    
    // If we are on a contour pixel, return the pixel
    // TODO: Is the float comparison that reliable? .5 is a tight margin,
    // maybe replace by .4 or .3.
    // TODO: If falls in "if" case, shouldn't calculate dist(...), b/c it's 0.
    if(length(sample.rg) > 0.0){
        if (sample.z < 0.5) return vec4(sample.xy,dist(sample.xy, coord),1.0);
        value = vec4(sample.xy,dist(sample.xy, coord),1.0);
    }

    vec2 offset = vec2(0, 0);
    float nextd = 0.0;

    // TODO: Remove all of the relaxation code.
    float eps = dist(vec2(1.0,2.0), vec2(0.0));
    vec2 relaxedCoord = vec2(0.0);
    float relaxedDist = 0.0;
    bool relax = false;

    // TODO: Investigate using distance to structure and not only one pixel.
    for(int i = -1; i < 2; i++) {
        for(int j = -1; j < 2; j++) {
            if (i==0 && j==0) continue;
            offset = vec2(step*i,step*j);
            sample = texture2D(s, (coord+offset)/vec2(iwidth, iheight)).rgb;
            nextd = dist(sample.xy, coord);

            relax = false;//(dist(sample.xy, value.xy) < eps);
            relaxedCoord = vec2((sample.x+value.x)*0.5, (sample.y+value.y)*0.5);
            relaxedDist = dist(relaxedCoord, value.xy);

            if(length(sample.rg) > 0.0) {
                value = (nextd < value.z) ? 
                    vec4(sample.x, sample.y, nextd, 1.0) : 
                    ((relax) ?
                        (((relaxedDist < value.z)) ? vec4(relaxedCoord.xy, relaxedDist, 1.0) : value ) :
                        value 
                    );
            }
        }
    }
    
    return value;
}