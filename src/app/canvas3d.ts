import { Shaders } from './shaders';
import * as THREE from 'three';
import * as poly2tri from 'poly2tri';
import { Point } from './point3d';

declare var require: any;

const enum Pass {
    Cleanup,       // 0
    Label,         // 1
    JumpFlood,     // 2
    MedialAxis,    // 3
    DistanceField, // 4
    Direct,        // 5
    SmoothBorder,  // 6
    SmoothAxis     // 7
}

export class Canvas3d {

    glCanvasElement : HTMLCanvasElement;
    d2CanvasElement : HTMLCanvasElement;

    private material : THREE.ShaderMaterial;
    private uniforms : any;

    private computeScene : THREE.Scene;
    private computeCamera : THREE.OrthographicCamera;

    private scene : THREE.Scene;
    private camera : THREE.OrthographicCamera;

    private renderer : THREE.WebGLRenderer;

    private width : number;
    private height : number;

    private turntable : boolean;
    private lastAnimationFrame : number;
    private inflated : THREE.Mesh;

    private c2dTexture : THREE.Texture;

    public borderBuffer : Float32Array;
    public medialBuffer : Float32Array;


    constructor (c3d : HTMLCanvasElement, c2d : HTMLCanvasElement) {
        this.glCanvasElement = c3d;
        this.d2CanvasElement = c2d;

        this.width = this.glCanvasElement.clientWidth;
        this.height = this.glCanvasElement.clientHeight;

        this.computeScene = new THREE.Scene();
        this.computeCamera = new THREE.OrthographicCamera(-1.0, 1.0, 1.0, -1.0);

        let rendererParameters = <THREE.WebGLRendererParameters>({
        canvas: this.glCanvasElement
        });
        this.renderer = new THREE.WebGLRenderer(rendererParameters);
        this.renderer.setSize( this.width, this.height );
        
        
        // import shaders
        let jfVert = require('./glsl/jf.vert')({});
        let jfFrag = require('./glsl/jf.frag')({});
        
        let t = new THREE.Texture(c2d);
        t.magFilter = THREE.NearestFilter;
        t.minFilter = THREE.NearestFilter;
        t.wrapS = THREE.RepeatWrapping;
        t.wrapT = THREE.RepeatWrapping;
        t.needsUpdate = true;
        this.c2dTexture = t;
        this.uniforms = {
            pass: new THREE.Uniform(Pass.Cleanup),
            sketch: new THREE.Uniform(this.c2dTexture),
            imgWidth: new THREE.Uniform(this.width),
            imgHeight: new THREE.Uniform(this.height),
            step: new THREE.Uniform(1)
        };
        let smp = <THREE.ShaderMaterialParameters>({
            uniforms: this.uniforms,
            vertexShader: jfVert,//s.vertex,
            fragmentShader: jfFrag//s.fragment
        });
        
        this.material = new THREE.ShaderMaterial(smp);
        let geometry = new THREE.PlaneBufferGeometry(2.0,2.0);
        let plane = new THREE.Mesh( geometry, this.material );
        this.computeScene.add( plane );

        this.computeCamera.position.z = 5;

        this.scene = new THREE.Scene();
        this.camera = new THREE.OrthographicCamera(-1.0, 1.0, 1.0, -1.0);
        this.camera.position.z = 10;
        var light2 = new THREE.PointLight( 0xefefff, 0.5, 100 );
		light2.position.copy(this.camera.position);
		this.scene.add( light2 );

        this.turntable = false;
    }

    getWidth () {
        return this.width;
    }

    getHeight () {
        return this.height;
    }

    getBorderDist () {
        return this.borderBuffer.filter((v, id) => {return id%4==2;});
    }

    getMedialAxisDist () {
        return this.medialBuffer.filter((v, id) => {return id%4==2;});
    }

    update3d (c : HTMLCanvasElement) {
        this.material.uniforms.sketch.value.needsUpdate = true;
        this.material.uniforms.pass.value = Pass.Label;
        this.material.uniforms.step.value = 0;
        this.renderer.render(this.computeScene, this.computeCamera);
    }

    jumpFlood () {
        let ping = new THREE.WebGLRenderTarget(this.width, this.height, {minFilter: THREE.NearestFilter, magFilter: THREE.NearestFilter, format: THREE.RGBAFormat ,type:THREE.FloatType, stencilBuffer: false});
        let pingU = new THREE.Uniform(ping.texture);
        let pong = new THREE.WebGLRenderTarget(this.width, this.height, {minFilter: THREE.NearestFilter, magFilter: THREE.NearestFilter, format: THREE.RGBAFormat ,type:THREE.FloatType, stencilBuffer: false});
        let pongU = new THREE.Uniform(pong.texture);
        let p = true;

        let dfBorder = new THREE.WebGLRenderTarget(this.width, this.height, {minFilter: THREE.NearestFilter, magFilter: THREE.NearestFilter, format: THREE.RGBAFormat ,type:THREE.FloatType, stencilBuffer: false});
        let dfBorderU = new THREE.Uniform(dfBorder.texture);
        let dfMedial = new THREE.WebGLRenderTarget(this.width, this.height, {minFilter: THREE.NearestFilter, magFilter: THREE.NearestFilter, format: THREE.RGBAFormat ,type:THREE.FloatType, stencilBuffer: false});
        let dfMedialU = new THREE.Uniform(dfMedial.texture);

        let n = Math.max(this.width, this.height);
        let iterations = Math.ceil(Math.log2(n));
        
        let stepsize = (s: number) => {
            return 2**(Math.ceil(Math.log2(n))-s-1);
        };
        /**/
        this.material.uniforms.pass.value = Pass.Label;
        this.material.uniforms.step.value = 0;
        this.renderer.render(this.computeScene, this.computeCamera, p?ping:pong);
        /**/       
        /**/
        this.material.uniforms.pass.value = Pass.JumpFlood;
        for(let i = 0; i<iterations; i++){
            p = !p;
            let step = stepsize(i);
            console.log(step);
            this.material.uniforms.step.value = step;
            this.material.uniforms.sketch = p?pongU:pingU;
            this.renderer.render(this.computeScene, this.computeCamera, p?ping:pong);
        }
        /**/
        /**/
        // Store distance info to border
        this.material.uniforms.pass.value = Pass.SmoothBorder;
        this.material.uniforms.sketch = p?pingU:pongU;
        this.renderer.render(this.computeScene, this.computeCamera, dfBorder);
        /**/
        /**/
        // Calculate medial axis
        p = !p;
        this.material.uniforms.pass.value = Pass.MedialAxis;
        this.material.uniforms.sketch = p?pongU:pingU;
        this.renderer.render(this.computeScene, this.computeCamera, p?ping:pong);
        /**/
        /**/
        // Clean medial axis
        p = !p;
        this.material.uniforms.pass.value = Pass.Cleanup;
        this.material.uniforms.sketch = p?pongU:pingU;
        this.renderer.render(this.computeScene, this.computeCamera, p?ping:pong);
        /**/
        /**/
        p = !p;
        this.material.uniforms.pass.value = Pass.Label;
        this.material.uniforms.sketch = p?pongU:pingU;
        this.renderer.render(this.computeScene, this.computeCamera, p?ping:pong);
        /**/
        /**/
        this.material.uniforms.pass.value = Pass.JumpFlood;
        for(let i = 0; i<iterations; i++){
            p = !p;
            let step = stepsize(i);
            console.log(step);
            this.material.uniforms.step.value = step;
            this.material.uniforms.sketch = p?pongU:pingU;
            this.renderer.render(this.computeScene, this.computeCamera, p?ping:pong);
        }
        /**/
        /**/
        // Store distance info to medial axis
        this.material.uniforms.pass.value = Pass.SmoothAxis;
        this.material.uniforms.sketch = p?pingU:pongU;
        this.renderer.render(this.computeScene, this.computeCamera, dfMedial);
        /**/
        p = !p;
        this.material.uniforms.step.value = 1;
        this.material.uniforms.pass.value = Pass.DistanceField;
        this.material.uniforms.sketch = dfBorderU;//p?pongU:pingU;
        this.renderer.render(this.computeScene, this.computeCamera);
        this.material.uniforms.sketch = new THREE.Uniform(this.c2dTexture);

        // read textures from GPU
        this.borderBuffer = new Float32Array(4*this.width*this.height);
        this.renderer.readRenderTargetPixels(dfBorder, 0, 0, this.width, this.height, this.borderBuffer);
        this.medialBuffer = new Float32Array(4*this.width*this.height);
        this.renderer.readRenderTargetPixels(dfMedial, 0, 0, this.width, this.height, this.medialBuffer);
    }

    buildMesh (contour : Array<Point>, steiner : Array<Point>, triangles : Array<poly2tri.Triangle>) {
        this.resetScene();

        let material = new THREE.MeshPhongMaterial( { color: 0xdddddd, specular: 0xffffff, shininess: 10, shading: THREE.SmoothShading, side: THREE.DoubleSide} );//new THREE.MeshBasicMaterial({color: 0x00ff00, side: THREE.DoubleSide, wireframe: true});//
		let geometry = new THREE.Geometry();

		//create common contour
		for (let i = 0; i < contour.length; i++) {
			geometry.vertices.push(
				new THREE.Vector3( contour[i].x,  contour[i].y, 0 )
			);
		};

		//create front vertices
		for (let i = 0; i < steiner.length; i++) {
			geometry.vertices.push(
				new THREE.Vector3( steiner[i].x,  steiner[i].y, steiner[i].z )
			);
		};

		//create back vertices
		for (let i = 0; i < steiner.length; i++) {
			geometry.vertices.push(
				new THREE.Vector3( steiner[i].x,  steiner[i].y, -steiner[i].z )
			);
		};

		//create faces
		let cl = contour.length;
        let sl = steiner.length;
        console.log(`contour length: ${cl}, steiner length: ${sl}`);
        let ptA : Point;
        let ptB : Point;
        let ptC : Point;
		
		for (let i = 0; i < triangles.length; i++) {
            ptA = <Point> triangles[i].getPoint(0);
            ptB = <Point> triangles[i].getPoint(1);
            ptC = <Point> triangles[i].getPoint(2);
			//create front faces
			geometry.faces.push( new THREE.Face3( ptA.id, ptB.id, ptC.id ) );

			//create back faces
			let a = (ptC.id>=cl)?(ptC.id+sl):(ptC.id);
			let b = (ptB.id>=cl)?(ptB.id+sl):(ptB.id);
			let c = (ptA.id>=cl)?(ptA.id+sl):(ptA.id);

			geometry.faces.push( new THREE.Face3( a, b, c ) );
		};

		geometry.verticesNeedUpdate = true;
        
        //
        geometry.normalize();
        
		//So as to allow smooth shading
		geometry.computeFaceNormals();
		geometry.computeVertexNormals();
        
		//Bounding sphere
        geometry.computeBoundingSphere();
        console.log(geometry.boundingSphere);

        this.inflated = new THREE.Mesh( geometry, material );
        this.scene.add(this.inflated);

        let sphGeom = new THREE.SphereGeometry(0.5);
        let sph = new THREE.Mesh(sphGeom, material);
        //this.scene.add(sph);

        this.turntable = true;
        this.render();//this.renderer.render(this.scene,this.camera);
        console.log("Rendered!");
    }

    resetScene() {
        this.turntable = false;
        if (this.lastAnimationFrame) cancelAnimationFrame(this.lastAnimationFrame);
        this.scene.remove(this.inflated);
    }

    render() {
        if(this.turntable){
            this.lastAnimationFrame = requestAnimationFrame(() => {this.render();});

            this.inflated.rotateY(0.01);

            this.renderer.render(this.scene, this.camera);
        }
    }
}
