export class Point implements poly2tri.XY {
    public x: number;
    public y: number;
    public z: number;
    public id: number;

    constructor(x: number, y: number, z?: number, id?: number) {
        this.x = x;
        this.y = y;
        this.z = z? z : 0;
        this.id = id? id : 0;
    }
}
