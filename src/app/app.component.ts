import { Canvas3d } from './canvas3d';
import { Canvas2d } from './canvas2d';
import { Inflate } from './inflate';
// https://stackoverflow.com/questions/21533757/three-js-use-framebuffer-as-texture

import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';

/**
 * This component encapsulates two canvases, one for 2d sketching and another for 3d viewing.
 */
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  
  /**
   * Common 
   */

  @ViewChild('content') content: ElementRef;

  public toggleCanvas = false;

  // After HTML page is loaded and available, initialize canvas manager classes
  ngAfterViewInit() {
    this.init2d();
    this.resize();
    this.init3d();
  }

  // Toggle between canvases
  change(){
    this.toggleCanvas = !this.toggleCanvas;
    this.c3d.resetScene();
  }

  resize(){
    let ctt : HTMLDivElement = this.content.nativeElement;
    this.c2d.resize(ctt.clientWidth, ctt.clientHeight);
  }

  /**
   * 2D 
   */

  @ViewChild('cvElement') cvElement : ElementRef;
  public c2d : Canvas2d;

  init2d() {
    this.c2d = new Canvas2d(this.cvElement.nativeElement);
  }

  /**
   * 3D 
   */

  @ViewChild('glElement') glElement : ElementRef;
  public c3d : Canvas3d;
  
  init3d() {
    this.c3d = new Canvas3d(this.glElement.nativeElement, this.c2d.canvas);
  }

  updateTex() {
    this.c3d.update3d(this.c2d.canvas);
  }

  runCalculation() {
    this.change();
    this.updateTex();
    this.c3d.jumpFlood();
    this.inflate();
  }

  inflate () {
    let inflate = new Inflate(
      this.c3d.getWidth(), 
      this.c3d.getHeight(), 
      this.c2d.getLastContour(),
      this.c3d.getBorderDist(),
      this.c3d.getMedialAxisDist(),
      this.c2d.getMask()
     );

    if(inflate.inflate()){
      this.c3d.buildMesh(inflate.contour, inflate.steiner, inflate.triangles);
    }
    else {
      this.change();
    }
  }

}
